npm install -g cordova ionic
ionic platform ios android
cordova plugin add https://github.com/songz/cordova-plugin-opentok

How to test?
- ionic serve
Browser will open, keep dev tools open
- ionic build ios (or android)
In platforms/ios you'll find xcode file
Android is a blacker magic for me than ios - https://github.com/driftyco/ionic-cli


When you run the app on ios/android take a look on web version, there are going to be such logs:
Someone connected
Someone disconnected
Someone connected

"Someone disconnected" means that plugin works wrong because it automatically disconnects person.
