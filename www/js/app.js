// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
    // Sign up for an OpenTok API Key at: https://tokbox.com/signup
    // Then generate a sessionId and token at: https://dashboard.tokbox.com
    var apiKey = "45215332"; // INSERT YOUR API Key

    var keys = [
      {
        sessionId: "1_MX40NTIxNTMzMn5-MTQyOTU2MDQyNjYzOX45R1BvNVRyTzdsbTlicTJmY0ZJQVl2RGV-fg",
        token: "T1==cGFydG5lcl9pZD00NTIxNTMzMiZzaWc9ZDU0M2M2NTFhZDdlMjI0MTllODI0NjAzMDhkM2Y0M2FkNTVmMjFhMjpyb2xlPXN1YnNjcmliZXImc2Vzc2lvbl9pZD0xX01YNDBOVEl4TlRNek1uNS1NVFF5T1RVMk1EUXlOall6T1g0NVIxQnZOVlJ5VHpkc2JUbGljVEptWTBaSlFWbDJSR1YtZmcmY3JlYXRlX3RpbWU9MTQyOTU2MDg0MSZub25jZT0wLjEzNDMwMDY3MjQ5NTAzMzg2JmV4cGlyZV90aW1lPTE0MzAxNjU0MDUmY29ubmVjdGlvbl9kYXRhPQ=="
      },
      {
        sessionId: "1_MX40NTIxNTMzMn5-MTQyOTU2MDcyODc1OX5mOUc4c29tRHhPak5PWVVjSlZnSHVtY2F-fg",
        token: "T1==cGFydG5lcl9pZD00NTIxNTMzMiZzaWc9Yzg0NTY3MzBkZGE5ZDllZDM1ZGJlYThiNDVmNDJhYjBlMGM2NWIzYzpyb2xlPXN1YnNjcmliZXImc2Vzc2lvbl9pZD0xX01YNDBOVEl4TlRNek1uNS1NVFF5T1RVMk1EY3lPRGMxT1g1bU9VYzRjMjl0UkhoUGFrNVBXVlZqU2xablNIVnRZMkYtZmcmY3JlYXRlX3RpbWU9MTQyOTU2MDc5OSZub25jZT0wLjUyMTg3MjU0MDg2MDE4OTcmZXhwaXJlX3RpbWU9MTQzMDE2NTQwNSZjb25uZWN0aW9uX2RhdGE9"
      },
      {
        sessionId: "1_MX40NTIxNTMzMn5-MTQyOTU2MjMxMzk1Mn5aV2VTMnJlYm42VHd3QVlFZHJudnBkdXd-fg",
        token: "T1==cGFydG5lcl9pZD00NTIxNTMzMiZzaWc9MWZkMWE2MDE3ZWMzMGVhZDM4Y2M5ZTQ5MzI5ZWI4NDUyMGVjMDNkODpyb2xlPXN1YnNjcmliZXImc2Vzc2lvbl9pZD0xX01YNDBOVEl4TlRNek1uNS1NVFF5T1RVMk1qTXhNemsxTW41YVYyVlRNbkpsWW00MlZIZDNRVmxGWkhKdWRuQmtkWGQtZmcmY3JlYXRlX3RpbWU9MTQyOTU2MjMyOCZub25jZT0wLjMyMTk2MTA4NTQ4OTc3NSZleHBpcmVfdGltZT0xNDMwMTY3MTAzJmNvbm5lY3Rpb25fZGF0YT0="
      }
    ];

    // Very simple OpenTok Code for group video chat
    window.sessions = [];
    window.messages = [];

    var i = 0;
    window.createSession = function() {
      if (i === 2) { return; }
      var session = TB.initSession(apiKey, keys[i].sessionId);
      session.connect(keys[i].token);
      session.on({
        connectionCreated: function(event) {
          var isMine = !event.connection ||
            (session.connection.id === event.connection.id);
          if (isMine) { return; }
          console.log('Someone connected');
        },
        connectionDestroyed: function(event) {
          var isMine = !event.connection ||
            (session.connection.id === event.connection.id);
          console.log('Someone disconnected');
          if (isMine) { return; }
        }
      });
      window.sessions.push(session);
      i++;
    };
    createSession();
    setTimeout(createSession, 5000);
  });


})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: "/tab",
    abstract: true,
    templateUrl: "templates/tabs.html"
  })

  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('tab.chats', {
      url: '/chats',
      views: {
        'tab-chats': {
          templateUrl: 'templates/tab-chats.html',
          controller: 'ChatsCtrl'
        }
      }
    })
    .state('tab.chat-detail', {
      url: '/chats/:chatId',
      views: {
        'tab-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/dash');

});
